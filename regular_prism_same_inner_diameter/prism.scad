module prism(height=1,radius1=1,radius2=1,outside=true,$fn)
{
	if(outside==true){
		cylinder(height,radius1/cos(180/$fn),radius2/cos(180/$fn),$fn);
	}else{
		cylinder(height,radius1,radius2,$fn);
	}
}

module prism_teardrop(height=1, radius1=1, radius2=1, outside=true, angle=0, fn)
{
	hull(){
		prism(height, radius1, radius2, outside, fn);
		translate([sin(angle)*1.41*radius1,0,0])
			cylinder(0.01,0.01,0.01);
		translate([sin(angle)*1.41*radius2,0,height])
			cylinder(0.01,0.01,0.01);
	}
}
