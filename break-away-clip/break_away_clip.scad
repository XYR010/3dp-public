$fn = 32;
module clip(
		clearance = [0,0,1] * 0.2)
{
	wedge();
	holder();
}

module wedge(
		clearance = [0,0,1] * -0.2,
		size = [10,10,3],
		angle = 60,
		radius = 2)
{
	difference(){
		hull(){
			translate([radius-size[0]/2,0,0])
				bicone(size[2],angle,radius+clearance[2]);
			translate([-radius+size[0]/2,radius-size[1]/2,0])
				bicone(size[2],angle,radius+clearance[2]);
			translate([-radius+size[0]/2,-radius+size[1]/2,0])
				bicone(size[2],angle,radius+clearance[2]);
			translate([-radius+size[0]/2,radius-size[1]/2,0])
				bicone(size[2],angle,radius+clearance[2]);
			translate([-radius+size[0]/2,-radius+size[1]/2,0])
				bicone(size[2],angle,radius+clearance[2]);
		}
		cylinder(size[2],radius,radius,center=true);
	}
}

module holder(
		clearance = [0,0,1] * -0.2,
		size = [32,14,3],
		wedge_size = [10,10,3],
		angle = 60,
		spring_size = [22,2,3],
		radius = 2)
{
	difference(){
		hull(){
			translate([(size[0]+wedge_size[0])/2-radius,0,0]-size/2)
				cube(size);
			translate([-5,0,0])
				wedge(angle=90);
		}
		translate([(size[0]+wedge_size[0])/2-radius,spring_size[1],0]-size/2)
			cube([spring_size[0],size[1]-2*spring_size[1],size[2]]);
		wedge(-clearance);
		cylinder(size[2],radius,radius,center=true);
		translate([-wedge_size[0],0,0])
			cylinder(size[2],wedge_size[1],wedge_size[1],$fn=6,center=true);
		translate([size[0]-radius,0,0])
			cylinder(size[2],radius,radius,center=true);
	}
}

module bicone(
		height = 3,
		angle = 60,
		radius = 2)
{
	union(){
		cylinder(height/2, radius+height/(2*tan(angle)), radius);
		mirror([0,0,1])
			cylinder(height/2, radius+height/(2*tan(angle)), radius);
	}
}

module clamp(
		clearance = [0,0,1] * -0.2,
		size = [5,14,3],
		thickness = 2)
{
	rotate([0,90,0]){
		difference(){
			cube(size+[0,2,2]*thickness, center=true);
			cube(size-[0,2,2]*clearance[2], center=true);
		}
	}
}

translate([0,0,1.5])
	clip();
translate([15.5,0,2.5])
rotate([0,0,90])
	clamp();
