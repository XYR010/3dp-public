use <../regular_prism_same_inner_diameter/prism.scad>

module eurocylinder(small_diameter=10,big_diameter=17,height=33,depth=25,slot_width=3,slot_height=3,normal_gap=0)
{
	$fn=32;
	union(){
		hull(){
			cylinder( depth, small_diameter/2+normal_gap, small_diameter/2+normal_gap);
			translate([ 0, height - (big_diameter + small_diameter)/2, 0])
				cylinder( depth, small_diameter/2+normal_gap, small_diameter/2+normal_gap);
		}
		cylinder( depth, big_diameter/2+normal_gap, big_diameter/2+normal_gap);
		translate([-slot_width/2-normal_gap,0,0])
			cube([slot_width+2*normal_gap,height+slot_height-big_diameter/2+normal_gap,depth]);
	}

}

module fastener(small_diameter=10,big_diameter=17,height=33,depth=25,fastener_height=10,fastener_depth=10,fastener_length=10+2*2,hole_height=6,normal_gap=0.2)
{
	difference(){
		translate([-fastener_length/2,height-big_diameter/2-hole_height-fastener_height/2,depth])
		hull(){
			cube([fastener_length,fastener_height,fastener_depth]);
			translate([-fastener_length/2,0,0])
			cube([fastener_length*2, fastener_height, 0.1]);
		}

		eurocylinder(depth=25+depth,normal_gap=normal_gap);
		translate([-fastener_length,height-big_diameter/2-hole_height,depth+fastener_depth/2])
		rotate([0,90,0])
			prism_teardrop(fastener_length*2,4/2+normal_gap,4/2+normal_gap, outside=true, angle=-60, fn=16);
			*teardrop(radius=4/2+normal_gap,fastener_length=fastener_length,angle=90);
	}
}

module eurocylinder_adapter()
{
	difference(){
		$fn=64;
		union(){
			cylinder(15,45/2,45/2);
			translate([0,0,15])
				cylinder(10,45/2,42/2);
			cylinder(2,53/2,55/2);
			translate([0,-(33-17)/2,0])
				fastener();
		}
		translate([0,-(33-17)/2,0])
			eurocylinder(normal_gap=0.2);
	}
}

module bolt(normal_gap=0)
{
	union(){
		rotate([90,45/2,0])
		cylinder(25,2-normal_gap,2-normal_gap,$fn=8);
		hull(){
			translate([3,0,0])
			rotate([90,45/2,0])
				cylinder(2,2-normal_gap,2-normal_gap,$fn=8);
			translate([-3,0,0])
			rotate([90,45/2,0])
				cylinder(2,2-normal_gap,2-normal_gap,$fn=8);
		}
	}
}

module euro_hull()
{
	difference(){
		eurocylinder(normal_gap=2);

		eurocylinder(normal_gap=0.4);
	}
}

*bolt();

module clip(length=60,height=3,depth=3,pin_diameter=3,pin_height=3,pin_number=5,pin_spacing=4,pin_offset=2.8)
{
	$fn=6;
	for(i=[0:pin_number-1]){
		translate([pin_offset+pin_spacing*i+pin_diameter/2,0,height])
			rotate([0,0,0])
				prism(pin_height,pin_diameter/2,pin_diameter/2,outside=false,$fn=6);
		translate([length-(pin_offset+pin_spacing*i+pin_diameter/2),0,height])
			rotate([0,0,0])
				prism(pin_height,pin_diameter/2,pin_diameter/2,outside=false,$fn=6);
	}
	translate([0,-depth/2,0])
		cube([length,depth,height]);
}

module switch_holder(total_height=8.9, button_height_pressed=4.58, travel=0.3, button_length=6, wallthickness=0.8, normal_gap=0.2)
{
	eurocylinder_small_diameter=10;
	difference(){
		cube([eurocylinder_small_diameter-2*normal_gap, eurocylinder_small_diameter-2*normal_gap, total_height-button_height_pressed/2-1-wallthickness-travel]);
		translate([(eurocylinder_small_diameter-button_length)/2-normal_gap,0,total_height-button_height_pressed-wallthickness])
		cube([button_length,button_length,button_height_pressed]);
		translate([0,0,0.8-wallthickness])
		difference(){
			translate([0,0,button_length/2])
				cube([eurocylinder_small_diameter,button_length,total_height]);
			translate([0,button_length/2,0])
			rotate([45,0,0])
				cube([eurocylinder_small_diameter,button_length/1.4142,button_length/1.4142]);
		}
	}
}

module actuator(inner_diameter=12.8, height=2, channel_width=3.2, wallthickness=0.8, travel=0.3, normal_gap=0.2)
{
	difference(){
		hull(){
			
			rotate([0,0,-135])
			mirror([0,1,0])
				cube([inner_diameter/2+wallthickness+normal_gap+travel,1,height]);
			rotate([0,0,-45])
				cube([inner_diameter/2+wallthickness+normal_gap+travel,1,height]);
			cylinder(2,inner_diameter/2+normal_gap+wallthickness,inner_diameter/2+normal_gap+wallthickness, $fn=64);
		}
		cylinder(2,inner_diameter/2+normal_gap,inner_diameter/2+normal_gap);
	}
	mirror([1,0,0])
	translate([(inner_diameter-channel_width)/2,-channel_width/2+normal_gap,0])
		cube([2,channel_width-2*normal_gap,height]);
	translate([(inner_diameter-channel_width)/2,-channel_width/2+normal_gap,0])
		cube([2,channel_width-2*normal_gap,height]);
}
clip();
